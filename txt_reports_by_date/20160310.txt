Date 10/03/2016
Time 19:50-23:00
Location Cambridge, UK
Altitude 12m
Lunar Phase Waxing crescent 9%
Temperature 3C (E 5 km/h)
Seeing 3 - Moderate seeing
Transparency 3 - Somewhat clear
Darkness 18.7
Telescopes Tele Vue 60 F6
Eyepieces Delos 12, Delos 8, PM2.5x

M42 Ori Neb 75x, 112x
At both these magnifications, I could not spot E and F. Again, some time at 112x I had the impression to discern F, but the view was too ambiguous. I believe the high level of humidity did not help despite the steady air.

Sigma Ori Mlt Star 112x
I tried hard to see whether it was possible to split A and C, but did not succeed. Next time.

Jupiter - Planet 45x, 75x, 112x
With the Delos 8mm (45x) I was able to see the two equatorial belts. Interestingly the view remained intact when the planet was moved near the eyepiece field stop. The belts were detectable without having to refocus. Unfortunately, due to the high humidity levels, I could not see the North and South temperate belts tonight even at higher magnifications (75x and 112x). They were  just invisible. The jovian moons were really sharp even at 112x (0.5mm e.p., 3.2mm f.l.). After many attempts, it results to me that there was a dark feature near the centre of the North Equatorial belt. Due to the poor transparency I am not sure whether this was an artefact of my eye or not, but I believe I saw something in there. No occultation as all the moons were visible. Possibly a large feston? As far as I know the GRS is in the South Equatorial belt, so this is also excluded. The mystery of the Dark Feature!

NGC2903 Leo Galaxy 30x, 45x
Star hopping from Epsilon Leo to Lambda Leo (Alterf). Then move slightly South. There is a rectangle and this target is just below the dimmer star of this rectangle. Invisible at 30x, whereas barely detectable at 45x. Due to the very high humidity, only a very faint shade of brighter gray was visible via averted vision. Jiggling the telescope did not help. To be sure it was detectable I had to go back to this rectangle a few times. Clearly no detail was visible, but just a very faint patch of gray. I want to go back to this target when transparency is better than tonight.


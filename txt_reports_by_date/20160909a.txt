Date 09/09/2016a
Time 0:30-1:30
Location Venice Area, Italy
Altitude 8m
Lunar Phase First Quarter 52%
Temperature 22C (NE 0 km/h)
Seeing 1 - Perfect seeing
Transparency 5 - Clear
Darkness 19
Telescopes SW 200 F6
Eyepieces ES 30mm, Docter 12.5mm

NGC752 And Opn CL 40x, 96x
Wonderful open cluster near Gamma And (Almaak).

NGC890 Tri Galaxy 40x, 96x
Very faint.

NGC925 Tri Galaxy 40x, 96x
Very faint.

NGC969 Tri Galaxy 40x, 96x
Very faint.

NGC891 And Galaxy 40x, 96x
Very faint.

NGC956 And Opn CL 40x, 96x
Not very interesting. Few stars.

M34 Per Opn CL 40x, 96x
Spiral cluster. Lovely.

M36 Aur Opn CL 96x
Nice one at 2mm e.p.

M37 Aur Opn CL 96x
My favourite of the trio. Nice one at 2mm e.p.

M38 Aur Opn CL 96x
Nice one at 2mm e.p.

M77 Cet Galaxy 40x, 96x
Easily visible.

NGC1055 Cet Galaxy 40x, 96x
Faint.

M74 Psc Galaxy 40x, 96x
{\color{red} Invisible}. sb: 14.13. I have no idea how Messier spotted this galaxy. Might he have spotted NGC660 instead?

NGC660 Psc Galaxy 40x, 96x
Faint.


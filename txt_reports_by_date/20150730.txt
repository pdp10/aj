Date 30/07/2015
Time 23:00-23:40
Location Cambridge, UK
Altitude 12m
Temperature 12C (wind: 0km/h)
Seeing 2 - Slight undulations
Transparency 3 - Somewhat clear
Telescopes Tele Vue 60 F6
Eyepieces Vixen 5mm SLV
Filters Variable Polarising Filter (VPF)

Moon - Satellite 72x + VPF
Full Moon. Cloudy sky, but the moon was visible from time to time. From Montes Apenninus, Eratosthenes at South, Aristoteles and Eudoxus at North. At far East of the Moon, Grimaldi and Riccioli. At North of the Moon, Plato. At North West, Atlas and Hercules. Finally I observed Theophilus in the centre-West.


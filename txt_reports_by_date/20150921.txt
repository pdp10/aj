Date 21/09/2015
Time 20:30-22:30
Location Cambridge, UK
Altitude 12m
Temperature 10C (5 km/h)
Seeing 3 - Moderate seeing
Transparency 4 - Partly clear
Telescopes Tele Vue 60 F6
Eyepieces Panoptic 24mm, Nagler 13mm, Nagler 7mm, Nagler 3.5mm, Powermate 2.5x
Filters Astronomik OIII

Mel186 Oph Opn CL 15x
Extended open cluster next to Beta Oph (Cebalrai). It is about 3 degrees large.

Alpha Cap Dbl Star 28x
Algedi. Nice and easy double star. A pair of doubles. The former were orange, the dimmer once looked grey.

Beta Cap Dbl Star 28x
Dabih. Another lovely double. The former is yellow, the other white-ish.

Omicron Cap Dbl Star 28x
If I remember they are both white.

M75 Sgr Glob CL 28x, 51x, 69x
A tedious star hopping from Omi Cap and hidden by a Moon at first quarter. This globular cluster was faint and 51x helped make it more visible by reducing the exit pupil. Not easy to find as there are no bright stars in the neighbourhood.

M31 And Galaxy 15x, 28x
Andromeda Galaxy. Core visible at 15x. Just a little bit of disc was detectable near the core at 28x.

M32 And Galaxy 15x, 28x
Visible at both 15x and 28x.

M110 And Galaxy 15x, 28x
I think I spotted it at 28x, but it was very faint.

NGC6543 Dra Pln Neb 28x, 51x, 69x +/- OIII
Cat's eye nebula. Star hopping from Zeta Dra (Aldhibah). This planetary nebula is quite bright. It was visible at 28x without filter and at 51x it appeared like a ball. An OIII filter help isolated it from the background stars although it was not necessary. I was a bit in a hurry due to clouds coming, but I think its colour was blue-ish.

IC348 Per Cl+Neb 28x
I tried to spot the associated cluster as this target is still too low for attempting the nebula. Unfortunately, I could not see the cluster either. It seemed a feasible target, but eventually something when wrong. I will try it again in Autumn.

NGC6229 Her Glob CL 28x, 51x
Star hopping from Phi and Tau Her. This globular cluster is relatively easy to find. It is quite dim, but still detectable particularly with averted vision. A soft faint grey patch. At 51x, it was more distinguishable from the background sky.

Alpha UMi Dbl Star 28x
Polaris. Companion visible at 28x. Very dim though and quite close to Polaris. Nice double. I like doubles with different brightness.


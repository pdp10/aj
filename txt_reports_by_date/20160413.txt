Date 13/04/2016
Time 21:30-22:13
Location Cambridge, UK
Altitude 12m
Lunar Phase Waxing crescent 42%
Temperature 13C (W 0 km/h)
Seeing 1 - Perfect seeing
Transparency 4 - Partly clear
Telescopes Tele Vue 60 F6
Eyepieces Vixen SLV 9, Delos 8, PM2.5x

Moon - Satellite 40x, 45x, 100x, 112x
I managed to observe the Moon a little bit longer than the other targets as this was free from clouds for a while. I spent about 20 minutes floating over our Satellite, magnificently dominating the sky. How many craters, how many minute details on its surface! A pure crystal image with gentle or craggy hills. A really lovely sight!

Jupiter - Planet 100x, 112x
I could detect the North temperate region but the sky transparency was not good due to high level of humidity. I could not spot features or irregularities on the equatorial belts.

M65 Leo Galaxy 45x
Leo triplet. Just detectable with direct vision. Even at this reduced exit pupil, the moon glare was just too much for observing more about this target..

M66 Leo Galaxy 45x
Leo triplet. As for M65.

Alpha Gem Dbl Star 100x
Castor. Perfect split. The two star discs were well distinct and separated. Unfortunately the sky was getting clouded, otherwise other double stars could have been viewed with this steady seeing.


Date 21/05/2016
Time 22:10-1:00
Location Venice Area, Italy
Altitude 8m
Lunar Phase Full Moon 100%
Temperature 19C (0 km/h)
Seeing 1 - Perfect seeing
Transparency 4 - Partly clear
Telescopes SkyWatcher 200 F6
Eyepieces Delos 12, Delos 8, Vixen SLV 5, Bresser SA 2x, PM2.5x

Mars - Planet 150x, 200x, 240x, 300x, 375x
Angular diameter: 18.3 arcsec, central-meridian latitude: 123. Homogeneous landscape covering Amazonis, Memnonia, and Tharsis. Near the border dark features were detectable but without a clear structure. These were slightly more visible at high power. A hint of North polar was also detectable.

Saturn - Planet 150x, 200x, 240x, 300x, 375x
Great view! The Cassini division was visible almost throughout the planet. At the border of the rings, it was a clear black line. On the planet it seemed that there was a bit of rings shadow. A tiny bit of planet shadow seemed to be projected on the back side of the rings. The equatorial belt was nicely observable and to me this was nicer at +300x magnifications. Titan and Rhea were well visible. Between Titan and Saturn there was Dione. Between Rhea and Saturn there was Thetys. Tethys and Dione were not straightforward to spot initially, but after a bit they were easily visible. I tried my best to spot Enceladus, but the Moon was just too bright.

Moon - Satellite 150x, 200x, 240x, 300x, 375x
Being full, I only observed the border at South-West. High magnifications worked well to reduce the Moon's glare. A wonderful flyby!


Date 10/02/2016
Time 19:00-23:00
Location Cambridge, UK
Altitude 12m
Lunar Phase Waxing crescent 5%
Temperature 2C (W 13 km/h)
Seeing 2 - Slight undulations
Transparency 5 - Clear
Darkness 19.22
Telescopes Tele Vue 60 F6
Eyepieces Panoptic 24, Nagler 13, Nagler 7, Nagler 3.5, PM2.5x

M42 Ori Neb 28, 51x, 69x, 103x, 129x
Orion's Nebula. I saw this target through this session. Possibly the best view I had tonight was at 69x. I tried to spot E and F in the Trapezium, but did not succeed.

Beta Ori Dbl Star 28, 51x, 69x, 103x, 129x
Rigel. Airy discs visible at all magnifications. The dimmer companion was detectable at 51x with difficulty. At higher magnifications this was obvious instead.

NGC1662 Ori Opn CL 28x, 51x
Open cluster of medium-small size located on Orion's shield. It shape is similar to an m letter.

Mel25 Tau Opn CL 15x
Hyades. Large open cluster dominated by Alpha Tau (Aldebaran).

M45 Tau Opn CL 15x
Pleiades. Wonderful open cluster formed by blue young stars. I could not spot the reflection nebulosity near Merope.

Alpha CMa Dbl Star 15x
Sirius. Just a quick look at this bright star.

M41 CMa Opn CL 15, 28x
Located at South from Alpha CMa (Sirius), this is a nice medium size open cluster. Worth a look.

Eta CMa Star 28x
Alundra. South East from Sirius. White blue star.There is a companion, although this is not a real double star. Quite pretty.

M31 And Galaxy 28x
Andromeda galaxy. Just the core was visible due to the light pollution coming from Cambridge.

M65 Leo Galaxy 28x, 51x
Leo triplet. Star hopping from Theta Leo (Chertan). After 20 minutes spent observing HIP55262, the closest bright star, I managed with difficulty to spot M65 and M66 with averted vision at 28x. Both of them appeared as elongated faint objects. My thought was that more aperture was required for these targets or at least a far darker sky than my current one. Counterintuitively, I decided to increase the magnification at 51x (1.2mm exit pupil), and both M65 and M66 were more distinguishable than earlier. I moved slightly away from the area and then returned for 6 times. Every time I spot them. This was not straightforward, but it was much more feasible than at 28x. The third galaxy forming the Leo Triplet, NGC3628, was simply invisible. I never managed to spot this. Likely a darker sky is required.

M66 Leo Galaxy 28x, 51x
See M65.

M81 UMa Galaxy 28x, 51x
Bode Galaxy. After spotting the Leo Triplet, or better Leo Doublet, I was curious to apply a high-ish power eyepiece on this target. Already at 28x the view was very pretty. An oval shape was clearly visible via direct vision, and I also had the impression that its core was distinguishable as a bit brighter than the rest. At 51x, it was a very nice target. No really more detail in it, but the increase in magnification made the object just a hint more appreciable.

M82 UMa Galaxy 28x, 51x
Cigar Galaxy. As for M81. Its structure was clearly elongated although its shape reminded me of a rectangle. At 51x, the image was very suggestive.

M97 UMa Pln Neb 51x
Owl nebula. A grey faint blob, but I managed to spot it without any filter.

M108 UMa Galaxy 51x
Surfboard galaxy. I could not spot it.

Jupiter - Planet 69x, 103x, 129x
Four moons visible, two equatorial belts clearly distinct whereas other two belts at the poles were just detectable. No GRS spotted if present. Wonderful view at 129x. The moons still showed a reduced Airy disc, and the belts were well defined.


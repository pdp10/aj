Date 18/09/2015
Time 21:00-22:45
Location Cambridge, UK
Altitude 12m
Temperature 11C (10 km/h)
Seeing 3 - Moderate seeing
Transparency 5 - Clear
Telescopes Tele Vue 60 F6
Eyepieces Panoptic 24mm, Nagler 13mm, Nagler 7mm, Nagler 3.5mm, Powermate 2.5x
Filters Astronomik OIII

NGC6960 Cyg SN Rem 15+OIII
Western Veil. Slightly visible near the star 52 Cyg. It appeared as a grey soft thin patch.

NGC6992 Cyg SN Rem 15+OIII
Eastern Veil. Easier to see with either averted or direct vision, although the border was not so defined.

IC1340 Cyg SN Rem 15+OIII
Souther-Eastern Veil. At the bottom of the Eastern veil, it was possible to discern a swollen nebula moving inside.

M31 And Galaxy 15x
Andromeda Galaxy. Faint patch. Just the nucleus was visible.

M15 Peg Glob CL 15x, 103x
Spottable at 15x. I pushed the magnification at 103x. No star was resolved.

NGC869/ 884 Per Opn CL 15x, 51x, 103x
Double Cluster. Lovely view at 51x. Both the complexes were in the same field. At 103x the two complexes could be studied separately.

Stock2 Cas Opn CL 15x, 51x
Large and sparse open cluster, but still with a pleasant structure. Many stars of similar brightness.

M57 Lyr Pln Neb 15x, 38x, 69x + OIII
Ring Nebula. The ring was visible with averted vision at 38x. At 69x the ring was easily observable.

M34 Per Opn CL 15x, 51x
Nice open cluster at both 15x and 51x. 15-20 stars were visible.

NGC752 And Opn CL 51x
Lovely open cluster located at South of Almaak.

Gamma And Dbl Star 51x
Almaak. Main component is Orange. Second component seemed grey.

NGC1502 Cam Opn CL 15x
Located at the end of the Kemble's cascade, this object is part of the group of stars forming Kemble1.

Milky Way - Galaxy 15x
I spanned from Cygnus to Cepheus, Cassiopeia and finally Perseus. Rich star fields and many clusters were observable or detectable on the way.


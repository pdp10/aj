Date 24/09/2015
Time 21:30-23:20
Location Cambridge, UK
Altitude 12m
Temperature 11C (13 km/h)
Seeing 2 - Slight undulations
Transparency 4 - Partly clear
Telescopes Tele Vue 60 F6
Eyepieces Nagler 13mm, Nagler 7mm, Nagler 3.5mm, Powermate 2.5x

Moon - Satellite 69x, 103x, 129x, 256x
Waxing Gibbous, 93 \%. The atmosphere was sufficiently steady for pushing the magnification. Interestingly, at 256x, I saw less floaters than at 103x. I think this is due to a very small exit pupil which essentially hid most of the floaters. Now wobbling on the surface due to bad seeing. At this magnification, it was possible to spot some roughness on the lunar surface which was not visible at lower magnifications. The image simply looked as if an antialiasing effect was applied. At 129x I had the most beautiful view: a considerable amount of details yet a crisp view. The positive thing of observing at these high magnification was the noticeable reduction of visible scatter light. Outstanding view also at 69x and 103x. At all magnifications, the terminator showed interesting features and several hills were observable beyond the terminator.

M57 Lyr Pln Neb 28x, 103x
Ring nebula. Easily detectable at 28x. At 103x the ring was visible, particularly with averted vision.

Beta Cyg Dbl Star 51x
Albireo. A wonderful double in the Summer Sky, easily visible with every telescope.

NGC6871 Cyg Opn CL 51x
This cluster is located on the Cygnus' neck and contains two double stars. One pair is actually a triple system with a dimmer third component (almost 10 mag). This dimmer component is not obvious at 51x but makes a nice picture with the other two pairs.

M45 Tau Opn CL 28x
Pleiades. Lovely view of this open cluster at 28x. The field is just perfect. I could spot about 15-20 dim stars.


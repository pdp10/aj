Date 29/06/2015
Time 21:30-22:00
Location Cambridge, UK
Altitude 12m
Temperature 21C (wind: 6km/h)
Seeing 2 - Slight undulations
Transparency 4 - Partly clear
Telescopes Tele Vue 60 F6
Eyepieces Vixen 5mm SLV, Nagler 3.5mm, Bresser SA 2x

Moon Oph Satellite 72x, 103x, 206x
Observation at twilight. Waxing Gibbous, 94\%. The moon was not very crisp tonight due to a little layer of high clouds caused by the high temperature during the day. I moved from Montes Apenninus to Copernicus. A small crater was visible inside, but many details on the circular border were not clear. Therefore I moved to Kepler as this was farther East hoping to improve the visible contrast. This showed a little shadow on one border. At 206x, from Kepler I moved North-East, following the crater chain formed by Kepler C, Marius D, F, A, C, and B. All these craters are relatively small and close to each other. Finally I moved North reaching Aristarchus which appeared beautiful. This white crater shows an impressive contrast and is close to a little half circle of hills at North-East and a dark crater (Herodotus) at East. Really nice view.


Date 26/08/2016
Time 21:00-23:00
Location Venice Area, Italy
Altitude 8m
Lunar Phase Waning Crescent 29%
Temperature 26C (E 0 km/h)
Seeing 1 - Perfect seeing
Transparency 5 - Clear
Darkness 18.8
Telescopes SW 200 F6
Eyepieces Docter 12.5mm, Vixen SLV 9mm, Baader VIP 2.0x

M3 CVn Glob CL 96x, 192x
Resolved some star.

M5 Ser Glob CL 96x
Resolved some star.

M13 Her Glob CL 96x, 192x
Great view due to the high position in the sky. A few stars were resolved. Majestic at 192x.

M92 Her Glob CL 96x
Great view. Resolved a few stars.

Saturn - Planet 96x, 192x, 266x
Cassini division and equatorial belt were visible. Titan, Thetis and Rhea were also visible.

Mars - Planet 96x, 192x
Syrtis Major was detectable. Mars was around 10 arcsec now.

Venus - Jupiter - Planet 96x
Very low in the sky. The two planets were overlapped.

Alpha UMi Dbl Star 96x
Easy split. Nice.

Alpha UMa Dbl Star 96x
Easy split. Nice.

Gamma UMa Mlt Star 96x
Nice view.


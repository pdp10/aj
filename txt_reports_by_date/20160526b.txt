Date 26/05/2016b
Time 22:20-23:00
Location Cambridge, UK
Altitude 12m
Lunar Phase Waning Gibbous 74%
Temperature 12C (E 10 km/h)
Seeing 3 - Moderate seeing
Transparency 4 - Partly clear
Darkness 18.74
Telescopes Tele Vue 60 F6
Eyepieces Vixen SLV 9, Vixen SLV 5, PM2.5x

Jupiter - Planet 40x, 72x, 100x, 180x
The daylight is getting longer as shown by the darkness measurement. Jupiter showed the two equatorial belts and the South polar region. I could not spot the GRS and in fact it was not out at this time (no false negative). I believe a festoon was discernable though. No moon event tonight. The best magnification was 72x.

Mars - Planet 40x, 72x, 100x, 180x
From the UK Mars is really low and the little aperture does not help. A white glitter was visible on the North pole suggesting a hint of the polar cap. I sometimes had the feeling to see a darker region on the West side of the planet, but the seeing was not good enough to be sure. The best magnification was 72x tonight.


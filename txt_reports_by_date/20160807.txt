Date 07/08/2016
Time 21:00-21:40
Location Cambridge, UK
Altitude 12m
Lunar Phase Waxing Crescent 22%
Temperature 19C (WSW 34 km/h)
Seeing 4 - Poor seeing
Transparency 5 - Clear
Telescopes Tele Vue 60 F6
Eyepieces Docter 12.5mm, Delos 12mm, Baader VIP 2.0x

Moon - Satellite 28.8x, 30x +/- VIP ~3x
The wind was too strong for comparing features between these two eyepieces. The views were just lovely without VIP, but unfortunately unstable at higher magnifications. Regarding colour, I did not see any major axial chromatic aberration (chromatic aberration of the first type) with the two eyepieces. Of note, my TV60 shows a minimal amount of colour fringe (purple fringe when the Moon is moved to the extreme right, yellow fringe when it is moved to the extreme left) with all my eyepieces. This because the TV60 is a doublet and not a triplet refractor. This colour fringe was very minimal and only noticeable near the edge. Both the Delos and the Docter showed minimal transverse chromatic aberration (chromatic aberration of the second type or lateral colour). Both the Docter and the Delos showed chromatic aberration of the exit pupil as a blue ring at the field stop. This blue ring seemed thinner to my eye. With the Baader VIP transverse chromatic aberration seemed almost removed on both the eyepieces. This test should be repeated with the dobson.


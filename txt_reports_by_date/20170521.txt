Date 21/05/2017
Time 10:00-11:30
Location Cambridge, UK
Altitude 12m
Temperature 19C (SW 13 km/h)
Seeing 3 - Moderate seeing
Transparency 5 - Clear
Telescopes Tele Vue 60 F6
Eyepieces Vixen NPL 30mm, Zeiss zoom 25.1-6.7mm, Vixen SLV 5mm and 9mm, Vixen HR 2.4mm, PM2.5x
Filters Lunt Herschel Wedge, Solar Continuum filter

Sun - Star 30x, 40x, 72x, 100x, 150x +/- SCF
Finally two (little) sunspots and a bit of granulation in white light. It's quite windy over here, but time to time the seeing settles just enough to catch some detail. The 2.4mm Vixen HR (150x) without Baader SC filter also allowed me to see the penumbra regions of those sunspots in those stable moments. Curiously, neither the SLV 9mm (40x) or the 9mm + PM2.5x (100x) with Baader SC filter managed to. Detection on AR12657. I'm watching it with the Vixen SLV 9mm plus powermate and I can just hardly spot it. Also invisible with the Vixen NLV 30mm + Powermate 2.5x + Baader solar continuum (SC). This is using the Baader solar continuum filter. Removed the solar continuum filter and I cannot even find it. The little penumbra regions around the larger sunspots are also lost. Going back to the Vixen HR 2.4mm now. I may try holding the Baader Solar Continuum in front of the eyepiece. I've just tried my Vixen SLV 5mm. I can detect the position with difficulty just because I know where it is, but no real detail is visible. The Vixen HR 2.4mm now shows a fifth dot. They form a kind of square with an external dot on the right. No Solar continuum filter was used for both the tests. The HR now shows 6 spots. The Zeiss zoom 25.1-6.7mm using Baader VIP 1.25" nosepiece as 1.25" adapter shows the same at about 70x. The Vixen SLV 5mm now shows 5 spots, one of which seems more a penumbra region. Again no SC was used. Another little dot with the Vixen HR 2.4mm. The larger sunspots on the left might miss some details because the seeing has deteriorated and it's cloudy now.


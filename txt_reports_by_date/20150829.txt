Date 29/08/2015
Time 11:30-13:00
Location Cambridge, UK
Altitude 12m
Temperature 21C (5-20 km/h)
Seeing 3 - Moderate seeing
Transparency 4 - Partly clear
Telescopes Tele Vue 60 F6
Eyepieces Nagler 7mm, Vixen 5mm SLV, Nagler 3.5mm
Filters Lunt Herschel Wedge, Orion Green Filter 58

Sun - Star 51x, 72x, 103x + GF58
Although the sky was a bit windy today and some haze was present from time to time, it was possible to see a large complex of sunspots located at the far East of the Equator. Around this complex there were faculae too. Three sunspots were also visible in the Southern Hemisphere. I could not see any penumbra region for these small sunspots. Granulation was detectable only at 72x today.


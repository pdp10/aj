Date 17/04/2016
Time 18:00-20:21
Location Cambridge, UK
Altitude 12m
Temperature 10C (W 11 km/h)
Seeing 3 - Moderate seeing
Transparency 4 - Partly clear
Telescopes Tele Vue 60 F6
Eyepieces Panoptic 24, Delos 12, Vixen SLV 9, Delos 8, Vixen SLV 5, PM2.5x
Filters Lunt Herschel Wedge, Orion Green filter n.58

Sun - Star 40x, 72x, 100x +/- GF58
The giant sunspot was now located in the Southern Hemisphere. The little nearby sunspots disappeared, but the big one was very similar to the previous week. The penumbra region was particularly extended and showed clear lines with and without GF58 filter. Best view possibly at 40x with this seeing. Granulation was also detectable as well as faculae at the borders.

Moon - Satellite 15x, 30x, 40x, 45x, 72x, 100x, 180x
The sky was still blue and the Moon emerged with its pale colour. To me the Moon is nicer at low power when the sky is not dark. The Panoptic 24 gave a wonderful 3D view.


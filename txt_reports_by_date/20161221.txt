Date 21/12/2016
Time 18:30-23:20
Location Venice Area, Italy
Altitude 8m
Lunar Phase Waning Crescent 39%
Temperature 5C (NNE 0 km/h)
Seeing 2 - Slight undulations
Transparency 5 - Clear
Telescopes SW 200mm F6
Eyepieces ES30, Docter 12.5, Zeiss zoom 25.1-6.7, Baader VIP 1.5x
Filters DGM NPB

M45 Tau Opn CL 40x, 96x
Lovely sight with the ES30 or the Docter UWA. I think the Docter showed some nebulosity around the brightest stars.

M35 Gem Opn CL 47.8x-179.1x, 142x
Great view with the zoom. Many stars popped up. This target was just amazing with the Docter + VIP 1.5x (142x).

M36 Aur Opn CL 47.8x-179.1x
Great view with the zoom. Many stars popped up.

M37 Aur Opn CL 47.8x-179.1x
Great view with the zoom. Many stars popped up.

M38 Aur Opn CL 47.8x-179.1x
Great view with the zoom. Many stars popped up.

M41 CMa Opn CL 96x, 47.8x-179.1x
Nice open cluster. Medium power recommended.

M77 Cet Galaxy 47.8x-179.1x
Easily spotted with dv.

M42 Ori Neb 40x, 96x, 47.8x-179.1x, 142x +/- DGM
Lovely sight. Clouds of gas of different intensity were visible. Border also showed high contrast. Trapezium: E and F stars were visible with dv. The DGM is just perfect on this target.

M43 Ori Neb 40x, 96x, 47.8x-179.1x, 142x +/- DGM
Nebula detected with filter and dv.

NGC1647 Tau Opn CL 47.8x-179.1x
Medium power recommended.

NGC1514 Tau Pln Neb 96x, 47.8x-179.1x
Just detected next to the star.

NGC2392 Gem Pln Neb 96x, 47.8x-179.1x
The filter enhanced the view, but not detail.

Alpha CMa Dbl star 96x, 47.8x-179.1x, 142x, zoom+vip
No split. Sirius was just too bright.

Sigma Ori Mlt star 47.8x-179.1x
Lovely sight.

Beta Ori Dbl star 96x, 47.8x-179.1x, 142x, zoom+vip
Easy split.

NGC2244 Mon CL+Neb 40x, 96x
Rosette neb and Satellite cluster.


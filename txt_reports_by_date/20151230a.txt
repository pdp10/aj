Date 30/12/2015a
Time 01:00-03:00
Location Venice Area, Italy
Altitude 8m
Temperature 2C (3 km/h)
Seeing 2 - Slight undulations
Transparency 5 - Clear
Telescopes Sky-Watcher Skyliner 200 F6
Eyepieces Panoptic 24, Nagler 13, Nagler 7, Nagler 3.5, PM2.5x

M48 Hya Opn CL 50x
Star hopping from Procyon. Moderately extended open cluster located between Mon and Hya. App mag: 5.5, about 300 million years old.

M67 Cnc Opn CL 50x, 92x
Star hopping from Zeta Hya. Not easy to find due to the Moon. At 92x the view was beautiful and revealed a few faint stars surrounding other bright stars. This cluster is the nearest old open cluster.

M3 CVn Glob CL 92x, 125x, 171x
Star hopping from Arcturus. Not easy to spot due to the Moon, location above the horizon and light pollution. The best view I achieved was at 125x. It revealed an amount of star granulation at the borders if observed carefully. This is the first globular cluster I saw with this telescope.

Moon - Satellite 125x, 171x
Phase 0.79. I spent most of the time observing Mare Nectaris and the near craters: Theophilus, Cyrillus, Catharina, and Fracastorius. Mare Nectaris is quite dark and shows internal long hills which are very beautiful to see. Its diameter is 860km and the mare was formed 3.8 billion years ago. The crater Theophilus is characterised by an internal mountain 1400m high, with 3-4 summits. The crater was formed from 3.2 and 1.1 billion years ago.

Jupiter - Planet 92x, 125x, 171x, 231x, 343x, 428x
The best view was at 125x and 171x. The seeing was not optimal though due to the rapid decrease in fog in the last two hours. The GRS was detectable at 3am with some difficulty. The polar belts were also visible but their observation required some time. Io was approaching the planet. I decided to push the magnification on Jupiter's moons. At 343x and 428x, the moons were larger but I do not believe it this feature is entirely real. After careful evaluation, these appeared like small bright circles with poorly definite borders, surrounded by a kind of halo similar to stars when out of focus. I think the bright circles are the moons, whereas the considerable amount of light surrounding these circles is due to a sort of Airy disc artefact, caused by high magnification.


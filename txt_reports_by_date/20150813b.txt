Date 13/08/2015b
Time 18:45-19:30
Location Venice Area, Italy
Altitude 25m
Temperature 40C (no wind)
Seeing 1 - Perfect seeing
Transparency 5 - Clear
Telescopes Tele Vue 60 F6
Eyepieces Nagler 13mm, Nagler 7mm, Vixen SLV 5mm, Nagler 3.5mm, Orion Barlow Shorty 2x
Filters Lunt Herschel Wedge, Orion Green Filter n.58

Sun - Star 28x, 51x, 72x, 103x, 144x, 206x + GF58
Three groups of sunspots. Two of these were at the level of the Equator, whereas the third, largest one, was located at South-East of the star. Penumbra regions were present on all these sunspots. Faculae were very prominent on the surrounding area of the third group of sunspots. Granulation was clear from 51x and higher magnification throughout the Sun surface. At 28x the view of the Sun was stunning, but at magnifications between 51x and 103x, sunspots, penumbrae, faculae and granulation were clear and evident. The green filter improved the view noticeably making small sunspots, faculae and granulation more obvious. In addition it removes a fair amount of brightness delivering a more relaxed observation.


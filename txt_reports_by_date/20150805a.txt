Date 05/08/2015a
Time 09:15-10:00
Location Venice Area, Italy
Altitude 25m
Temperature 30C (no wind)
Seeing 1 - Perfect seeing
Transparency 5 - Clear
Telescopes Tele Vue 60 F6
Eyepieces Nagler 7mm
Filters Lunt Herschel Wedge, Orion Green Filter n.58

Sun - Star 51x+GF58
This was the first time I observed the Sun with an Orion green filter. Granulation improved noticeably and the contrast between sunspots and solar surface was largely enhanced. I did not find the observation of the Sun in green as distracting and I think the green filter will replace the variable polarising filter in the future.


Date 12/05/2015
Time 21:00-23:45
Location Cambridge, UK
Altitude 12m
Temperature 6C (wind: 20km/h)
Seeing 2 - Slight undulations
Transparency 5 - Clear
Telescopes Tele Vue 60 F6
Eyepieces TV Panoptic 24mm, Nagler 13mm, Nagler 7mm, Nagler 3.5mm, Bresser SA 2x

Jupiter Cnc Planet 103x, 206x
Observed in the twilight. Still visible at 206x with some detail but the new tripod is not up to this sort of magnifications. To be fair, the new tripod was fine at 103x but only when there was no wind. 3 belts and 4 moons visible. It would be interesting to try 206x with my solid tripod.

Venus Gem Planet 103x
Observed in the twilight. Visible 60\% of its phase. No cloud detail was detectable. A polarised filter might help on this target.

M44 Cnc Opn CL 15x, 28x
The Nagler 13mm offers the best view. Its fov covers the whole object nicely. Image not degraded at all and the background sky was darkened just the right amount for maximising contrast. Exit pupil of about 2.0mm shows a really nice brightness / contrast for point source DSO.

NGC869/ 884 Per Opn CL 15x, 28x
Double Cluster. Again, the Nagler 13 offering almost 3 degrees of fov shows the full object with great detail but conserving an adequate image brightness.

Stock2 Cas Opn CL 15x, 28x
As above.

M103 Cas Opn CL 28x
Not sure whether I saw M103 or NGC457 (the Owl Cluster) though. A clear double star was well visible and there were a few dim stars in the background were also detectable. This object starts being visible at 28x. It is relatively small, but a lovely target. I think it was M103 as my memory seems more similar to the images.

M60 Vir Galaxy 15x, 28x
Turn West to Vindemiatrix. A little crown of star is visible slightly South. Continue and you see a little arrow of stars and a single star in the North. M60 is between these two objects. Not detectable at 15x. Detectable via averted vision at 28x. A patch of grey. The sky was not fully dark though and my eye was not dark adapted. I believe this object can show more detail.

27 Hya Dbl Star 15x, 28x
Easily split. Colours detectable.

M13 Her Glob CL 15x, 28x
Detectable at 15x, but nicer at 28x. No star was resolved. It would be interesting to try 51x although I guess this might be too much.

M57 Lyr Pln Neb 15x, 28x, 51x
Ring Nebula. For the first time, I managed to see this object with the TV-60. I find extremely difficult to detect it at 15x unless I map the nearby stars with Stellarium. At 28x M57 is clearly visible and appears as a grey blob. At 51x the ring is detectable. I did not try to use an OIII filter because I was freezing due to lack of cloths and about to leave. I believe this target will show much more detail at 51x with OIII filter.

Saturn Sco Planet 103x
Very low on the horizon and therefore not the best moment for viewing this target. Despite this, rings and titan were visible. Neither the Cassini division nor belts were detectable.


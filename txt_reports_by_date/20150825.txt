Date 25/08/2015
Time 23:00-23:30
Location Cambridge, UK
Altitude 12m
Temperature 14C (27 km/h)
Seeing 4 - Poor seeing
Transparency 2 - Poor
Telescopes Tele Vue 60 F6
Eyepieces Nagler 13mm, Vixen SLV 5mm, Powermate 2.5x

Moon - Satellite 69x, 72x
Waxing Gibbous, phase: 0.80\%. Very cloudy sky and the Moon was visible at moments. It was still possible to compare the Vixen SLV 5mm with the Tele Vue Nagler 13mm + Powermate 2.5x. This is only a preliminary test due to the bad sky condition. As expected the N13+PM2.5x showed a wonderful larger field of view, where a detailed Moon was floating in space. I am very happy with it, as this was the first reason why I decided to buy the PM2.5x. Possibly the Nagler showed a bit more light scattered than the Vixen, although this was a bit difficult to analyse because the Moon tonight was surrounded by clouds which were only visible through the Nagler due to its larger field of view. In terms of details on the Moon surface, I could not see obvious differences. Both the contenders showed a very crisp image of the Moon. When the Moon was near the edge (+90\%), the Nagler showed a little yellow border on the Moon disc facing the edge. This was minimal and did not really affected the view to my eye.


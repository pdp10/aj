Date 22/11/2016
Time 21:45-23:00
Location Cambridge, UK
Altitude 12m
Lunar Phase Waning Crescent 31%
Temperature 7C (SSE 10 km/h)
Seeing 3 - Moderate seeing
Transparency 5 - Clear
Darkness 19.6
Telescopes Tele Vue 60 F6
Eyepieces Panoptic 24, Docter 12.5, Vixen SLV 9mm, 5mm
Filters DGM NPB, Astronomik OIII

M42 Ori Neb 15x, 28.8x +/- DMG NPB or OIII
The DMG NPB shows far more nebula extension than the OIII or filterless. Stars are both green and red, but the nebula's colour is quite neutral. This filter seems very promising.

Beta Ori Dbl Star 28.8x, 40x, 72x
Companion detected at 40x. Easy at 72x. Invisible at 28.8x.

Sigma Ori Dbl Star 28.8x, 40x
6 stars visible.

Cr69 Ori Opn CL 28.8x, 40x
Open cluster surrounding Meissa. Lovely at 28.8x.

M35 Gem Opn CL 15x, 28.8x, 40x
Lovely view at all magnifications.

M36 Aur Opn CL 15x, 28.8x, 40x
Quite nice at 40x although this requires darker skies.

M37 Aur Opn CL 15x, 28.8x, 40x
At 40x many stars were visible, particularly using AV.

M38 Aur Opn CL 15x, 28.8x, 40x
Very nice to see this again.

M45 Tau Opn CL 15x, 28.8x
At 28.8x they almost fit the fov of the Docter. I had a feeling to spot some nebula around Merope and the other two brightest stars.

Mel25 Tau Opn CL 15x, 28.8x
Very suggestive at 28.8x.

M81 UMa Galaxy 28.8x
This is not well placed at the moment, but it is still detectable as a round grey patch.

M82 UMa Galaxy 28.8x
Not well placed at the moment, but still detectable as a grey line.


Date 24/12/2016
Time 18:30-0:20
Location Venice Area, Italy
Altitude 8m
Lunar Phase Waning Crescent 21%
Temperature 6C (WSW 19 km/h)
Seeing 2 - Slight undulations
Transparency 5 - Clear
Telescopes SW 200mm F6
Eyepieces ES30, Docter 12.5, Zeiss zoom 25.1-6.7, Baader VIP 1.5x
Filters DGM NPB

Venus - Planet 96x, 47.8x-179.1x
Half planet was visible. Dark area near the terminator. No feature on the atmosphere.

Mars - Planet 96x, 47.8x-179.1x
Very small. Slightly darker feature near the centre of the planet.

Epsilon Lyr Dbl star 96x, 47.8x-179.1x
Double double. Both stars were split at about 100x.

M57 Lyr Pln Neb 96x, 47.8x-179.1x +/- DGM
Ring nebula. Ring structure visible with DV at all mag.

Alpha UMi Dbl Star 40x
Easy split.

M29 Cyg Opn CL 96x
Cooling tower.

NGC6910 Cyg Opn CL 96x
Nice little open cluster above Gamma Cyg (Sadr). I found this more beautiful than M29. Coloured stars.

M42 Ori Neb 40x, 96x, 47.8x-179.1x, 142x +/- DGM
Clouds revealed more details with increase in magnification. Trapezium: E and F stars were visible with dv.

M43 Ori Neb 40x, 96x, 47.8x-179.1x, 142x +/- DGM
Nebula detected with filter and dv.

NGC1977 Ori Neb 40x, 96x, 47.8x-179.1x, 142x +/- DGM
Running man nebula. Easily visible with DV.

IC418 Lep Pln Neb 47.8x-179.1x + DMG
Spirograph nebula. Easily visible with DV using the DMG. Round shape, no detail.

M35 Gem Opn CL 47.8x-179.1x, 142x
Great view with the zoom. Many stars popped up. This target was just amazing with the Docter + VIP 1.5x (142x).

M41 CMa Opn CL 96x, 47.8x-179.1x
Nice open cluster. Medium power recommended.

Alpha CMa Dbl star 96x, 47.8x-179.1x, 142x, zoom+vip
I think I spotted Sirius B this time. This target requires a small fov eyepiece. Sirius should be placed just outside the field stop in order to limit its glare. Sirius B appeared as a faint dot hidden in the rays coming from Sirius. I saw this dot with the Docter plus VIP first, but the Zoom plus VIP made this detection much easier due to the smaller fov.

NGC2244 Mon Opn CL 40x, 96x, 47.8x-179.1x
Satellite cluster. The best starting point for open clusters in the area.

NGC2254 Mon Opn CL 40x, 96x, 47.8x-179.1x
Very small open cluster. I barely detected at 96x. The zoom helped quite a lot.

NGC2251 Mon Opn CL 40x, 96x, 47.8x-179.1x
Nice medium-small open cluster with some coloured stars.

NGC2264 Mon CL+Neb 40x, 96x, 47.8x-179.1x
Cone nebula. I could not see the nebula, but focused on the Christmas tree.

Alpha Gem Dbl Star 96x
Castor. Easy split.


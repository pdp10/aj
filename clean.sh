#!/bin/bash

# This file is part of AstroJournal.
#
# AstroJournal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AstroJournal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AstroJournal.  If not, see <http://www.gnu.org/licenses/>.
 


# remove old unused filed.
git rm latex_reports_by_date/*.tex
git rm latex_reports_by_target/*.tex
git rm latex_reports_by_constellation/*.tex
git rm sgl_reports_by_date/*.txt
git commit -m 'Clean up'
git push origin master


